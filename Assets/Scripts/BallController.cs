using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    private Rigidbody ballRigidbody;

    public int power;

    private void Start()
    {
        ballRigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        ballRigidbody.AddForce(Vector3.back * power, ForceMode.Force);
    }
}
