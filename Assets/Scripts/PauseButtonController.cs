using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButtonController : MonoBehaviour
{
    public GameObject pausePanel;

    public void onPauseButtonClicked()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }
}
