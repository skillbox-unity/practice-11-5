using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueButtonController : MonoBehaviour
{
    public GameObject pausePanel;

    public void onContinueButtonClicked()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
    }
}
